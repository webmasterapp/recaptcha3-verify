<?php

/** @noinspection PhpUnused */
/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

declare(strict_types=1);

namespace MasterApp\ReCaptchaVerify;
use Exception;
use JsonException;
use MasterApp\Mattermost\MattermostException;
use Nette\Http\IRequest;
use Tracy\Debugger;

/**
 * Class Communicator
 * @package MasterApp\Networking
 */
class Communicator {

    private Logger $logger;

    private bool $debugModeEnabled;

    private ?string $reCaptchaSecret = null;

    private const reCaptchaVerifyUrl = 'https://www.google.com/recaptcha/api/siteverify';

    public function setDebugModeEnabled(bool $debugModeEnabled): void { $this->debugModeEnabled = $debugModeEnabled; }

    public function setSecretReCaptchaSecret(?string $secret): void { $this->reCaptchaSecret = $secret; }

    /** @param Logger $logger */
    public function __construct(Logger $logger) { $this->logger = $logger; }

    /**
     * @param string $token
     * @param string $clientIp
     * @param IRequest|null $request
     * @param float|null $reportScoreLowerThan
     * @return float
     * @throws ReCaptchaVerifyCommunicatorSystemException
     * @throws ReCaptchaVerifyCommunicatorReCaptchaSecretNotSetException
     * @throws ReCaptchaVerifyCommunicatorWrongResponseException
     */
    public function verifyRecaptcha(string $token, string $clientIp, ?IRequest $request = null, ?float $reportScoreLowerThan = null): float {

        // Init debug
        $debugObject = new DebugObject();
        $debugObject->token = $token;
        $debugObject->clientIp = $clientIp;

        // Input
        if ($this->reCaptchaSecret === null) { throw new ReCaptchaVerifyCommunicatorReCaptchaSecretNotSetException('Please set reCaptcha secret in config'); }
        $requestData = ['secret' => $this->reCaptchaSecret, 'response' => $token];
        $requestData['remoteip'] = $clientIp;

        // Init
        $oCurlInstance = curl_init(self::reCaptchaVerifyUrl);
        if ($oCurlInstance === false) { throw new ReCaptchaVerifyCommunicatorSystemException('Curl initialization failed'); }
        curl_setopt($oCurlInstance, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($oCurlInstance, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($oCurlInstance, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($oCurlInstance, CURLOPT_TIMEOUT, 10);
        curl_setopt($oCurlInstance, CURLOPT_CONNECTTIMEOUT, 10);

        // Process
        $result = curl_exec($oCurlInstance);
        $code = curl_getinfo($oCurlInstance, CURLINFO_HTTP_CODE);
        $message = curl_errno($oCurlInstance) ? ' (' . curl_error($oCurlInstance) . ')' : '';
        $tryDecode = null;
        if (is_string($result)) {
            try { $tryDecode = json_decode($result, true, 512, JSON_THROW_ON_ERROR); }
            catch (Exception) {}
        }

        // Debug
        $debugObject->code = $code;
        try {$debugObject->result = ! empty($result) ? json_encode($result, JSON_THROW_ON_ERROR) : null; } catch (JsonException) {

        }
        $debugObject->curlError = ! empty($message) ? $message : null;
        $debugObject->curlTotalTime = (round(curl_getinfo($oCurlInstance, CURLINFO_TOTAL_TIME) * 1000)) . 'ms';
        $debugObject->curlFirstTime = (round(curl_getinfo($oCurlInstance, CURLINFO_STARTTRANSFER_TIME) * 1000)) . 'ms';
        $debugObject->action = $tryDecode['action'] ?? null;
        $actionString = $debugObject->action ?? '-';
        if ($this->debugModeEnabled) { Debugger::barDump($debugObject, "ReCaptcha({$actionString})"); }

        // Error
        if (! isset($tryDecode['score'], $tryDecode['action']) || ! is_array($tryDecode) || $code !== 200 || isset($tryDecode['error-codes'])) {

            try {$this->logger->generateReCaptchaLog($debugObject, $request, false); } catch (JsonException | MattermostException $e) {
                throw new ReCaptchaVerifyCommunicatorSystemException('Impossible to generate log', 0, $e, $debugObject);
            }

            $exception = new ReCaptchaVerifyCommunicatorWrongResponseException('Error while trying to verify reCaptcha');
            $exception->debugObject = $debugObject;
            throw $exception;
        }

        // Success
        try { $this->logger->generateReCaptchaLog($debugObject, $request, ! ($reportScoreLowerThan !== null && $tryDecode['score'] < $reportScoreLowerThan)); }
        catch (JsonException | MattermostException $e) {
            throw new ReCaptchaVerifyCommunicatorSystemException('Impossible to generate log', 0, $e, $debugObject);
        }

        return $tryDecode['score'];
    }
}

/**
 * Class ReCaptchaVerifyCommunicatorCurlInitException
 * @package MasterApp\ReCaptchaVerify
 */
class ReCaptchaVerifyCommunicatorSystemException extends DebugObjectException {}

/**
 * Class ReCaptchaVerifyCommunicatorReCaptchaSecretNotSetException
 * @package MasterApp\ReCaptchaVerify
 */
class ReCaptchaVerifyCommunicatorReCaptchaSecretNotSetException extends DebugObjectException {}

/**
 * Class ReCaptchaVerifyCommunicatorWrongResponseException
 * @package MasterApp\ReCaptchaVerify
 */
class ReCaptchaVerifyCommunicatorWrongResponseException extends DebugObjectException {}