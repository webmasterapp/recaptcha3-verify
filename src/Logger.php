<?php

declare(strict_types=1);

/** @noinspection PhpUnused */

namespace MasterApp\ReCaptchaVerify;
use DateTime;
use JsonException;
use MasterApp\Mattermost\Mattermost;
use MasterApp\Mattermost\MattermostException;
use Nette\Http\IRequest;
use RuntimeException;
use stdClass;

/**
 * Class Logger
 * @package MasterApp\Networking
 */
class Logger {

    public string $logPath;

    public string $version;

    public bool $debugModeEnabled;

    private const Line = '----------------------------------------------------------------------------------------------------';

    private Mattermost $mattermost;

    public function setLogPath(string $logPath): void { $this->logPath = $logPath; }

    public function setVersion(string $version): void { $this->version = $version; }

    public function setDebugModeEnabled(bool $debugModeEnabled): void { $this->debugModeEnabled = $debugModeEnabled; }

    /** @param Mattermost $mattermost */
    public function __construct(Mattermost $mattermost) {
        $this->mattermost = $mattermost; }

    private function setUpLogDir(): string {
        $logPathWithDir = $this->logPath . '/reCaptcha/';
        if (! file_exists($logPathWithDir) && ! mkdir($logPathWithDir, 0777, true) && ! is_dir($logPathWithDir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $logPathWithDir));
        }
        return $logPathWithDir;
    }

    /**
     * @param DebugObject $debugObject
     * @param IRequest|null $request
     * @param bool $success
     * @throws JsonException
     * @throws MattermostException
     */
    public function generateReCaptchaLog(DebugObject $debugObject, ?IRequest $request = null, bool $success = true): void {

        if ($request !== null && $this->checkIsMattermost($request)) { return; }
        $logPathWithDir = $this->setUpLogDir();

        $action = $debugObject->action ?? 'none';
        $curlError = $debugObject->curlError ?? 'without http error';
        $encodedResult = json_encode($debugObject->result, JSON_THROW_ON_ERROR);
        $debug = $this->getRequestDebugObject($request);
        $debugMode = $this->debugModeEnabled ? 'debugMode' : 'prodMode';

        $message = "** :apierror: reCaptcha error thrown **\n";
        $message .= "Please review the error output.\n";
        $message .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n\n";
        $message .= "| Type              | Info                                                |
                     | :---------------- |:---------------------------------------------------:|
                     | Version/ENV       | {$this->version} / {$debugMode}                         | 
                     | HTTPCode/Error    | {$debugObject->code} / {$curlError}                     |
                     | ClientIP          | {$debugObject->clientIp}                              |
                     | URL               | {$debug->url}                                         |
                     | Client IP         | {$debug->remote}                                      |
                     | Action            | {$action}                                             |
                     | Token             | `{$debugObject->token}`                               |
                     | Body              | {$debug->body}                                        |
                     | Headers           | `{$debug->headers}`                                   |
                     | CurlResult        | `{$encodedResult}`                                    |\n";

        if (! $success) { $this->mattermost->sendMessage($message, true); }

        // Log locally
        $outputData = "ReCaptcha log\n";
        $outputData .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n";
        $outputData .= json_encode($debugObject, JSON_THROW_ON_ERROR);
        $outputData .= "\n\n" . self::Line . "\n";
        $fileName = $success ? (new DateTime())->format('Y-m-d') . '-OK.txt' : (new DateTime())->format('Y-m-d') . '-ERR.txt';
        file_put_contents($logPathWithDir . $fileName, $outputData, FILE_APPEND);
    }

    /**
     * @param IRequest $request
     * @return bool
     */
    private function checkIsMattermost(IRequest $request): bool {

        $agent = $request->getHeader('user-agent');
        return empty($agent) || str_contains($agent, 'mattermost');
    }

    /**
     * @param IRequest|null $request
     * @return stdClass
     * @throws JsonException
     */
    private function getRequestDebugObject(?IRequest $request = null): stdClass {

        $debug = new stdClass();
        $debug->url = $request?->getUrl();
        $debug->remote = $request?->getRemoteAddress();
        $debug->headers = $request !== null ? json_encode($request->getHeaders(), JSON_THROW_ON_ERROR) : null;
        $debug->body = $request !== null ? json_encode($request->getRawBody(), JSON_THROW_ON_ERROR) : null;
        return $debug;
    }
}
