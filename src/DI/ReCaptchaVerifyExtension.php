<?php

declare(strict_types=1);

namespace MasterApp\ReCaptchaVerify\DI;
use MasterApp\ReCaptchaVerify\Communicator;
use MasterApp\ReCaptchaVerify\Logger;
use Nette\DI\CompilerExtension;

/**
 * Class ReCaptchaVerifyExtension
 * @package MasterApp\ReCaptchaVerify
 */
class ReCaptchaVerifyExtension extends CompilerExtension {

    public function loadConfiguration(): void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('reCaptchaVerifyLogger'))
            ->setType(Logger::class)
            ->addSetup('setVersion', [$config['version'] ?? 'Undefined'])
            ->addSetup('setLogPath', [$config['logPath']])
            ->addSetup('setDebugModeEnabled', [$config['debugModeEnabled']]);

        $builder->addDefinition($this->prefix('reCaptchaVerifyCommunicator'))
            ->setType(Communicator::class)
            ->addSetup('setDebugModeEnabled', [$config['debugModeEnabled']])
            ->addSetup('setSecretReCaptchaSecret', [$config['reCaptchaSecret'] ?? null]);
    }
}
