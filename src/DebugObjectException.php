<?php

declare(strict_types=1);

namespace MasterApp\ReCaptchaVerify;
use Exception;
use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * Class DebugObjectException
 * @package MasterApp\Logger
 */
abstract class DebugObjectException extends Exception {

    public ?DebugObject $debugObject = null;

    /**
     * DebugObjectException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param DebugObject|null $debugObject
     */
    #[Pure] public function __construct(string $message = '', int $code = 0, Throwable $previous = null, DebugObject $debugObject = null) {

        parent::__construct($message, $code, $previous);
        $this->debugObject = $debugObject;
    }
}