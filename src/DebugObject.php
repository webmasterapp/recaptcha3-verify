<?php

declare(strict_types=1);

namespace MasterApp\ReCaptchaVerify;

/**
 * Class DebugObject
 * @package App\Models\Logger
 */
class DebugObject {

    public int $code;

    public ?string $result;

    public ?string $curlError;

    public string $curlTotalTime;

    public string $curlFirstTime;

    public string $token;

    public string $clientIp;

    public ?string $action;
}